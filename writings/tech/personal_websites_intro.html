<!DOCTYPE html>
<link rel="stylesheet" href="../../style.css" />
<html>

  <head>
      <meta charset="utf-8" />
      <!-- Titre dans la bare de navigation & search results -->
      <title>Lokenstein - Personal Websites & Digital Gardens</title>
  </head>

  <body>

    <div class="container">

      <!-- Header -->
      <div class="header">
        <h1><a class="main_title" href="../../index.html">Lokenstein's</a></h1>
        <ul>
          <li><a class="lia" href="../../about/me.html">ABOUT</a></li>
          <li><a class="lia" href="../writing_main.html">WRITING</a></li>
          <li><a class="lia" href="../../links/links_main.html">LINKS</a></li>
          <li><a class="lia" href="../../comics/comics_main.html">COMICS</a></li>
          <li><a class="lia" href="../../fanfics/fanfics_main.html">FANFICS</a></li>
        </ul>
      </div>

      <div id="right_post" class="right_post">
        <h2 id="post_title">Personal Websites & Digital Gardens</h2>
        <p class="date"> Created: 03/08/2024 - Last updated: 21/09/2024</p>
        <div class="right_inside">
          <p>
              This page is an introduction to personal websites & digital gardens: what they are, why you should create one, and a list of a few I enjoy to inspire you.</br>
          </p>

          <p>
            <ul class="list_block">
              <li class="list2"><a href="#intro">Personal Websites: An Introduction</a></li>
              <li class="list2"><a href="#why">Why create a personal website?</a></li>
              <li class="list2"><a href="#start">Where to start?</a></li>
              <li class="list2"><a href="#inspiration">Get inspired!</a></li>
              <li class="list2"><a href="#recs">Some digital gardens I enjoy</a></li>
            </ul>
          </p>
        </div>
      </div>

      <!-- 
      Thoughts

      Add a part about streams vs gardens vs campires?
      The web's current obsessions with reverse chronological streams (blogs + social medias)
      The need to review our thoughts instead of slapping them on the web 2sec after thinking them?
       -->

      <div id="left_post" class="left_post">
        <h2 id="intro">Personal Websites: An Introduction</h2>
        <div class="left_inside">

          <p>
            What are personal websites and what can they look like? Well, a personal website is quite self-explanatory: it's a website usually created and maintained by one person, who uses it for whatever purpose they see fit. But a personal website can be many things: a blog, a static website, a digital garden... wait, what do these mean?
          </p>
          <p>
            You're probably familiar with blogs: they're usually a series of chronological articles which aren't meant to be modified after publication, and which visitors can like or comment on. 
          </p>
          <p>
            However, this page won't be much about blogs (or wikis, or web rings, or the thousand other types of websites you'll discover if you're willing to go down this rabbit hole), but will mostly focus on static personal websites, and more specifically static digital gardens.
          </p>
          <p>
            Static websites used to be the norm on the Internets before web 2.0: they are pages that are coded in HTML & CSS only, so that even though they might contain articles and pictures and all sorts of things, the visitors cannot interract with the website much at all. Likes and comments don't exist, and most static websites don't have stats about how many visitors they get. This also means no trackers and cookies, no advertisement, no algorithms and no AI - which I personally think is a big plus! 
          </p>
          <p>
            Digital gardens, now, are often static websites, although they don't necessarily have to be. The basic idea of digital gardens is that they're not streams of posts that are quickly forgotten like social medias or most blogs, but a place where things are here to stay and to be regularly updated, taken care of. In the words of <a href="https://maggieappleton.com/garden-history" target="_blank">Maggie Appleton</a>:
          </p>
          <p>
            <blockquote cite="https://maggieappleton.com/garden-history">    
            A garden is a collection of evolving ideas that aren't strictly organised by their publication date. They're inherently exploratory – notes are linked through contextual associations. They aren't refined or complete - notes are published as half-finished thoughts that will grow and evolve over time. They're less rigid, less performative, and less perfect than the personal websites we're used to seeing. It harkens back to the early days of the web when people had fewer notions of how websites "should be.” It's an ethos that is both classically old and newly imagined.</blockquote>          
          </p>          
          <p>
            The website you're currently reading this article from is a static, digital garden website. If you want to react to an article, you'll have to send me an <a href="mailto:lokenstein@tuta.io">email</a> or a <a href="https://weirder.earth/@lokenstein" target="_blank">toot</a>, and most pages are constantly evolving. For instance, you'll notice that on many of my articles there's not only a creation date, but also a "last updated" date, as the content of most pages evolve with time.
          </p>
          <p>
            Now that's been cleared, let's talk about why you should create a website!
          </p>

        </div>
      </div>

      <div id="right_post" class="right_post">
        <h2 id="why">Why create a personal website?</h2>
        <div class="right_inside">

          <h3>Decorate your own corner of the internet...</h3>

          <p>
            When I decided to make myself a website, I was curious about self-coded static websites for their freedom - both in terms of customization but also because they're outside of the corporate web - but what really convinced me was <a href="https://ritualdust.com/" target="_blank">Lizbeth Poirier</a>'s opening quote in their "<a href="https://ritualdust.com/craft/make-yourself-a-website/" target="_blank">Make yourself a website</a>" article:
          </p>
          <p>
            <blockquote cite="https://ritualdust.com/craft/make-yourself-a-website/">    
            The same way as I can’t imagine living in a space designed and decorated by someone else based off a generic pattern, I can’t imagine my words and my work exist solely in the conformist and reductive spaces that are current day social media apps. </blockquote>          
          </p>
          <p>
            And indeed, how come I create whole calendars and journals from blank notebooks, I make (some of) my own furniture and clothes, but when it comes to the Internets, suddenly I fall into premade templates and the restrictions that comes from ready-made blogs and social media?
          </p>

          <h3>... and fill it with your favorite things!</h3>

          <p>
            We're so used to the premade templates and restrictions of the current mainstream web, that creating your own website from scratch can be not only mind blowing... but also a bit overwhelming! Where should you start? How should you organize what you write? What are you even going to write about? 
          </p>

          <p>
            In my opinion, there's truly only one way to answer these questions, and it's to start your own website! You website will look like <em>you</em>, and what it contains will depend on what you're interested in, what you usually write or collect, what recommendations or playlists you give to friends, how you organize your arts and crafts, and so on.
          </p>
          <p>
            For instance, as a person who always carries many notebooks and collects addresses, recipes and urls, it doesn't come as a surprise that my website contains lists of my <a href="../world_exploration/turku.html" target="_blank">favorite places</a>, as well as some <a href="../food/mocktails.html" target="_blank">mocktail recipes</a> and <a href="../../fanfics/fanfics_main.html" target="_blank">fanfiction recommendations</a>.
          </p>
          <p>
            But that's just what I decided to put on <em>my</em> website. Others have <a href="https://dogboy.smol.pub/candyfloss" target="_blank">pictures of the art they create</a>, <a href="https://azaliz.me/english/book-rec.html" target="_blank">lists of their favorite books</a>, <a href="https://sebsauvage.net/comprendre/" target="_blank">tech advice</a> or <a href="https://cosmolinguist.dreamwidth.org/" target="_blank">journal entries</a>. Some people have <a href="https://rosano.hmm.garden/" target="_blank">very simple layouts</a>, while others have <a href="https://www.toomanybees.com/" target="_blank">layouts so artistic</a> that it becomes the main focus of their site.
          </p>
          <p>
            It's this freedom that I find really amazing in personal websites! Without thinking of coding skills or anything, ask yourself for a second: if you had a website that could look like anything, and contain anything, what would you do with it?
          </p>

        </div>
      </div>

      <div id="left_post" class="left_post">
        <h2 id="start">Where to start?</h2>
        <div class="left_inside">

          <p>
            Now, I realize that the idea of coding your own website can be quite daunting. But a static website only requires HTML and CSS knowledge, and with just a few lessons you can already do a lot with those!
          </p>
          <p>
            Personally, I host my code for free on the non profit service <a href="https://codeberg.org/" target="_blank">Codeberg</a>, I code my website by myself on Sublime Text and I upload my changes with git in command line. Except for the Codeberg part, my process might not be very beginner friendly because IT used to be my job, but bellow are some extra resources to get you started.
          <p>
            <ul class="list_block">
              <li class="list2"><a href="https://maggieappleton.com/nontechnical-gardening" target="_blank">Digital Gardening for Non-Technical Folks</a>, by Maggie Appleton</li>
              <li class="list2"><a href="https://nesslabs.com/digital-garden-set-up" target="_blank">How to set up your own digital garden</a>, by Anne-Laure Le Cunff</li>
              <li class="list2"><a href="https://azaliz.me/english/creating-static-website.html" target="_blank">How to create your own static website</a>, by Azaliz</li>
              <li class="list2"><a href="https://ritualdust.com/craft/make-yourself-a-website/#practical-knowledge-to-get-you-started" target="_blank">More resources to get you started</a>, by Lizbeth Poirier on Ritual Dust</li>
            </ul>
          </p>

        </div>
      </div>

      <div id="right_post" class="right_post">
        <h2 id="inspiration">Get inspired!</h2>
        <div class="right_inside">
          
          <p>
            Do you need more motivation to start your own website, or do you simply want to read more about the subject? I got you! Here are some articles and videos which encouraged me to create this website, and continue to motivate me throughout the years.

            <ul class="list_block">
              <li class="list2"><a href="https://maggieappleton.com/garden-history" target="_blank">A Brief History & Ethos of the Digital Garden</a>, by <a href="https://maggieappleton.com/" target="_blank">Maggie Appleton</a> - this is my favorite essay about digital gardening, I always feel very inspired whenever I re-read it!</li>
              <li class="list2"><a href="https://ritualdust.com/craft/make-yourself-a-website" target="_blank">Make Yourself A Website</a>, by <a href="https://ritualdust.com" target="_blank">Lizbeth Poirier on Ritual Dust</a> - a short article on the importance of creating your own little corner of the Internet</li>
              <li class="list2"><a href="https://rosano.hmm.garden/01etwe6d05s2c8m8fj53wh3s4w" target="_blank">Digital garden</a>, by <a href="https://rosano.hmm.garden/" target="_blank">Rosano</a> - a very brief digital garden manifesto</li>
            </ul>
          </p>

          <!-- 
          To Read
          http://www.eastgate.com/garden/Enter.html
          https://reallifemag.com/the-great-offline/
          https://nesslabs.com/mind-garden
          https://thecreativeindependent.com/essays/laurel-schwulst-my-website-is-a-shifting-house-next-to-a-river-of-knowledge-what-could-yours-be/
          https://branch.climateaction.tech/
          https://tomcritchlow.com/2019/02/17/building-digital-garden/
          https://tomcritchlow.com/2018/10/10/of-gardens-and-wikis/
          https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/ (https://www.youtube.com/watch?v=ckv_CjyKyZY)
          https://maggieappleton.com/cozy-web
          https://joelhooks.com/digital-garden
          https://stackingthebricks.com/how-blogs-broke-the-web/
          https://www.swyx.io/digital-garden-tos
          https://indieweb.org/2020/Pop-ups/Garden-And-Stream
          https://www.swyx.io/learn-in-public
          https://www.mentalnodes.com/the-only-way-to-learn-in-public-is-to-build-in-public
          https://maggieappleton.com/bidirectionals

          https://egghead.io/lessons/egghead-sector-the-future-of-mdx-and-digital-gardens
          https://github.com/MaggieAppleton/digital-gardeners?tab=readme-ov-file      
          https://www.buildingasecondbrain.com/
           -->

        </div>
      </div>

      <div id="left_post" class="left_post">
        <h2 id="recs">Some digital gardens I enjoy</h2>
        <div class="left_inside">

          <p>
              Please note that I have <a href="./personal_websites_list.html">a full page</a> dedicated to personal website recommendations, but this is just a smaller and more accessible version of that, focused only on digital gardens!
          </p>

          <p>
            <ul class="list_block">
              <li class="list"><a href="https://azaliz.me/" target="_blank">Azaliz's page</a> [EN / FR]</li>
              <li class="list"><a href="https://bisonaari.github.io/garden/index.html" target="_blank">Bison's den</a> [EN]</li>
              <li class="list"><a href="https://dogboy.smol.pub/" target="_blank">Dogboy's page</a> [EN]</li>
              <li class="list"><a href="https://rosano.hmm.garden/" target="_blank">Rosano's page</a> [EN]</li>
            </ul>
          </p>

          <!-- 
          To Check
          https://tomcritchlow.com/wiki/
          http://archive.rhizome.org/
          https://www.davidkoh.co/
          http://daywreckers.com/
          https://wiki.xxiivv.com/site/about.html
          https://webring.xxiivv.com/#random
          https://gwern.net/index
          https://notes.andymatuschak.org/About_these_notes
          https://100r.co/site/home.html
          https://www.mentalnodes.com/antistatic-garden

          Lots of links here (A list of interesting websites to get lost in - section) https://ritualdust.com/craft/make-yourself-a-website/#practical-knowledge-to-get-you-started
           -->

        </div>
      </div>

    </div>
  </body>
</html>
